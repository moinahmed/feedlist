package com.moin.feedlist;

import com.moin.feedlist.utils.Helper;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    /**
     * Following testcase accepts date in yyyy-MM-dd'T'HH:mm:ssZ
     * Output should be in EEE hh:mma format
     * Unit test will return date in local timezone format.
     * i.e. if 2015-06-26T12:03:29+10:00
     * It should return Fri 12:03PM
     */
    @Test
    public void test_dateConversion(){
        String output = Helper.changeDateFormat("2015-06-26T12:03:29+10:00");
        assertEquals("Fri 12:03PM",output);
    }
}