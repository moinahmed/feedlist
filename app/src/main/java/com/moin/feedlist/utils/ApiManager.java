package com.moin.feedlist.utils;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This contains all network calls with api end-points.
 * Central place makes it easy to manage and modify it.
 * All activities will call methods from this ApiManager class.
 * ApiManager class uses VolleySingleton to make network requests.
 */

public class ApiManager {

    public static final String BASE_URL = "https://stage.airtasker.com/android-code-test";
    public static final String FEED_URL = BASE_URL+"/feed.json";
    public static final String TASK_URL = BASE_URL+"/task";
    public static final String PROFILE_URL = BASE_URL+"/profile";

    public interface JsonArrayCallback {
        void onSuccess(JSONArray jsonArray);
        void onError(VolleyError volleyError);
    }

    public interface JsonObjCallback {
        void onSuccess(JSONObject jsonObj);
        void onError(VolleyError volleyError);
    }
    /**
     * This makes network call to fetch Feed on main screen.
     * @param ct - calling activity context.
     * @param callback - call back to get results back in JsonArray format when success.
     */

    public static void getFeedApi(Context ct, final JsonArrayCallback callback){
        JsonArrayRequest jsonArrayReq = new JsonArrayRequest(FEED_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if(callback != null)
                            callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                if(callback != null)
                    callback.onError(error);
            }
        });

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        //Volley does retry for you if you have specified the policy.
        jsonArrayReq.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(ct).addToRequestQueue(jsonArrayReq);
    }

    /**
     * This will fetch data for profile, it requires profile id fetched from Feed API.
     * @param ct - application context
     * @param profileId - profile id returned in Feed API
     * @param callback - it returns JsonObject format on success.
     */
    public static void getProfileApi(Context ct, int profileId, final JsonObjCallback callback){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(PROFILE_URL+"/"+profileId+".json", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(callback != null)
                            callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                if(callback != null)
                    callback.onError(error);
            }
        });
        VolleySingleton.getInstance(ct).addToRequestQueue(jsonObjReq);
    }

    /**
     * This will fetch data for task, it requires task id fetched from Feed API.
     * @param ct - application context
     * @param taskId - task id returned in Feed API
     * @param callback - it returns JsonObject format on success.
     */
    public static void getTaskApi(Context ct, int taskId, final JsonObjCallback callback){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(TASK_URL+"/"+taskId+".json", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(callback != null)
                            callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                if(callback != null)
                    callback.onError(error);
            }
        });
        VolleySingleton.getInstance(ct).addToRequestQueue(jsonObjReq);
    }
}
