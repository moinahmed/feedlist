package com.moin.feedlist.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.kaopiz.kprogresshud.KProgressHUD;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Helper {

    /**
     * date conversion function, it changes date to local format
     * @param dateStr
     */
    public static String changeDateFormat(String dateStr){
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
        originalFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        DateFormat targetFormat = new SimpleDateFormat("EEE hh:mma", Locale.UK); //UK local for small am/pm. US will print AM/PM
        targetFormat.setTimeZone(TimeZone.getDefault());
        String targetDateStr = "";
        try {
            Date date = originalFormat.parse(dateStr);
            targetDateStr = targetFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetDateStr;
    }

    //common alert popup to display error messages
    public static AlertDialog alertDialog;
    public static void showAlertDialog(Context ct, String title, String message, final DialogInterface.OnClickListener callback){
        if(alertDialog != null && alertDialog.isShowing()){
            return;
        }
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ct);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(callback != null){
                    callback.onClick(dialogInterface, i);
                }
            }
        });
        alertDialog = alertDialogBuilder.create();
        if (ct != null && ct instanceof Activity) {
            Activity activity = (Activity)ct;
            if ( activity.isFinishing()) {
                return;
            }
        }
        alertDialog.show();
    }


    //progress indicator methods to show and hide progress HUD.
    public static KProgressHUD progressHUD;
    public static synchronized void showProgressHUD(Context ct){
        if(progressHUD == null){
            progressHUD = KProgressHUD.create(ct)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Please wait...")
                    .setCancellable(false)
                    .setAnimationSpeed(1)
                    .setDimAmount(0.5f);
        }
        progressHUD.dismiss();
        progressHUD.show();
    }

    public static synchronized void hideProgressHUD(){
        if(progressHUD != null){
            progressHUD.dismiss();
        }
    }

}
