package com.moin.feedlist.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.moin.feedlist.R;
import com.moin.feedlist.adapters.FeedAdapter;
import com.moin.feedlist.models.FeedItem;
import com.moin.feedlist.models.Profile;
import com.moin.feedlist.models.Task;
import com.moin.feedlist.utils.ApiManager;
import com.moin.feedlist.utils.Helper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;

public class MainActivity extends AppCompatActivity {

    private RecyclerView feedRecyclerView;
    private FeedAdapter feedAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private List<FeedItem> feedDataList = new ArrayList<>(); //stores actual feed data to be rendered.
    private Gson gson; // Json library reference to parse json response.
    private Realm realm; //DB reference.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        setupFeedRecyclerView();
        setupSwipeDownGesture();
        fetchFeedData(); //starting point to load feed data
    }

    private void setupFeedRecyclerView(){
        feedRecyclerView = findViewById(R.id.feed_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        feedRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        feedRecyclerView.setLayoutManager(layoutManager);

        // specify an adapter
        feedAdapter = new FeedAdapter(feedDataList);
        feedRecyclerView.setAdapter(feedAdapter);

    }

    /**
     * Pull down Feed List to refresh data.
     * Refer #10 Add swipe down gesture on feed list
     */
    private void setupSwipeDownGesture(){
        final SwipeRefreshLayout swipeView = findViewById(R.id.swipeView);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchFeedData();
                swipeView.setRefreshing(false);
            }
        });
    }

    /**
     * This function will start the main feed api call which will be followed with Profile and Task API calls.
     */
    private void fetchFeedData(){
        Helper.showProgressHUD(this); //show progress bar
        ApiManager.getFeedApi(this, new ApiManager.JsonArrayCallback() {
            @Override
            public void onSuccess(JSONArray jsonArray) {
                constructFeedData(jsonArray);
            }

            @Override
            public void onError(VolleyError volleyError) {
                Helper.showAlertDialog(MainActivity.this, "Error", "Pull down to refresh "+volleyError.getMessage(), null);
            }
        });
    }

    /**
     * Iterates over feed data and fetch subsequent information related to it
     * @param jsonArray - data from Feed api.
     */
    private void constructFeedData(JSONArray jsonArray){
        feedDataList = new ArrayList<>(); //reset entries
        for(int i=0; i<jsonArray.length(); i++){
            JSONObject obj = jsonArray.optJSONObject(i);

            FeedItem feedItem = gson.fromJson(obj.toString(), FeedItem.class);

            if (feedItem.getTaskId() == 0 || feedItem.getProfileId() == 0)
                continue; //skip row

            feedDataList.add(feedItem);
            setTaskToFeedItem(feedItem);
            setProfileToFeedItem(feedItem);
        }
        feedAdapter.replaceData(feedDataList);
        Helper.hideProgressHUD(); //hide progress bar
    }

    /**
     * Fetch task related information for individual row
     * @param feedItem - individual feed row
     */
    private void setTaskToFeedItem(final FeedItem feedItem){
        //check if task is already fetched to avoid unnecessary network call.
        Task task = realm.where(Task.class).equalTo("id", feedItem.getTaskId()).findFirst();
        if (task != null){
            updateTaskDetails(feedItem, task);
            return;
        }

        //task entry doesn't exist in local storage, fetch from server.
        ApiManager.getTaskApi(this, feedItem.getTaskId(), new ApiManager.JsonObjCallback() {
            @Override
            public void onSuccess(JSONObject jsonObj) {
                Task task = gson.fromJson(jsonObj.toString(), Task.class);
                persistModelToDB(task);
                updateTaskDetails(feedItem, task);
            }

            @Override
            public void onError(VolleyError volleyError) {
                Helper.showAlertDialog(MainActivity.this, "Error", "Pull down to refresh "+volleyError.getMessage(), null);
            }
        });
    }

    /**
     * Fetch profile related information for individual row
     * @param feedItem - individual feed row
     */
    private void setProfileToFeedItem(final FeedItem feedItem){
        //check if profile is already fetched to avoid unnecessary network call.
        Profile profile = realm.where(Profile.class).equalTo("profileId", feedItem.getProfileId()).findFirst();
        if (profile != null){
            updateProfileDetails(feedItem, profile);
            return;
        }

        //profile entry doesn't exist in local storage, fetch from server.
        ApiManager.getProfileApi(this, feedItem.getProfileId(), new ApiManager.JsonObjCallback() {
            @Override
            public void onSuccess(JSONObject jsonObj) {
                Profile profile = gson.fromJson(jsonObj.toString(), Profile.class);
                updateAvatarDetails(profile);
                updateProfileDetails(feedItem, profile);
                persistModelToDB(profile);
            }

            @Override
            public void onError(VolleyError volleyError) {
                Helper.showAlertDialog(MainActivity.this, "Error", "Pull down to refresh "+volleyError.getMessage(), null);
            }
        });
    }

    /**
     * Replaces placeholder {taskName} with actual task name.
     * @param feedItem - individual feed row
     * @param task - Task objection related to the feed row
     */
    private void updateTaskDetails(FeedItem feedItem, Task task){
        feedItem.setTask(task);
        String updatedText = feedItem.getText().replace("{taskName}", "\"" + task.getName() + "\"");
        feedItem.setText(updatedText);
        feedAdapter.notifyDataSetChanged();
    }

    /**
     * Replaces placeholder {profileName} with actual name. Also constructs avatar url.
     * @param feedItem - individual feed row
     * @param profile - Task objection related to the feed row
     */
    private void updateProfileDetails(FeedItem feedItem, Profile profile){
        //update feedItem text
        feedItem.setProfile(profile);
        String updatedText = feedItem.getText().replace("{profileName}", profile.getFirstName());
        feedItem.setText(updatedText);
        feedAdapter.notifyDataSetChanged();
    }

    //update profile avatar
    private void updateAvatarDetails(Profile profile){
        if (!profile.getAvatarMiniUrl().isEmpty())
            profile.setAvatarMiniUrl(ApiManager.BASE_URL+profile.getAvatarMiniUrl());
    }

    /**
     * Store model to DB for future use.
     * @param obj - it can be Profile or Task or any Realm DB model.
     */
    private void persistModelToDB(final RealmObject obj){
        realm.beginTransaction();
        realm.copyToRealm(obj);
        realm.commitTransaction();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close(); //close db on activity close.
    }
}
