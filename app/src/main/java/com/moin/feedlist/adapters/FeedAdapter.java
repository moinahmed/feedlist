package com.moin.feedlist.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.moin.feedlist.R;
import com.moin.feedlist.models.FeedItem;
import com.moin.feedlist.utils.Helper;
import com.moin.feedlist.utils.VolleySingleton;

import java.util.List;

/**
 * FeedAdapter class will be used in recyclerView to render feed data in list view
 */
public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedViewHolder> {
    private List<FeedItem> mDataset;

    public FeedAdapter(List<FeedItem> data){
        mDataset = data;
    }

    //method to replace old data with new one
    public void replaceData(List<FeedItem> data){
        mDataset.clear();
        mDataset.addAll(data);
        notifyDataSetChanged();
    }

    public static class FeedViewHolder extends RecyclerView.ViewHolder{
        private ImageView thumbImg;
        private TextView titleView;
        private TextView timeView;
        private TextView categoryView;
        private ImageLoader mImageLoader;

        public FeedViewHolder(@NonNull View v) {
            super(v);
            thumbImg = v.findViewById(R.id.thumb_img);
            titleView = v.findViewById(R.id.title_tv);
            timeView = v.findViewById(R.id.time_tv);
            categoryView = v.findViewById(R.id.category_tv);
            mImageLoader = VolleySingleton.getInstance(v.getContext()).getImageLoader();
        }
    }

    @NonNull
    @Override
    public FeedAdapter.FeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowLayout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_row, parent, false);

        FeedViewHolder fh = new FeedViewHolder(rowLayout);
        return fh;
    }

    @Override
    public void onBindViewHolder(@NonNull FeedAdapter.FeedViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        FeedItem feedItem = mDataset.get(position);

        if (feedItem.getTask() != null && feedItem.getProfile() != null){
            String url = feedItem.getProfile().getAvatarMiniUrl();
            if (!url.isEmpty())
                holder.mImageLoader.get(url,ImageLoader.getImageListener(holder.thumbImg, R.drawable.placeholder_profile,R.drawable.placeholder_profile));
            else
                holder.thumbImg.setImageResource(R.drawable.placeholder_profile); //set default url
            holder.titleView.setText(feedItem.getText());
            holder.timeView.setText(Helper.changeDateFormat(feedItem.getCreatedAt()));
            holder.categoryView.setText(feedItem.getEvent());
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
