package com.moin.feedlist.models;

import io.realm.RealmObject;

public class Profile extends RealmObject {

    private int profileId;
    private String avatarMiniUrl;
    private String firstName;
    private int rating;

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public String getAvatarMiniUrl() {
        return avatarMiniUrl;
    }

    public void setAvatarMiniUrl(String avatarMiniUrl) {
        this.avatarMiniUrl = avatarMiniUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
