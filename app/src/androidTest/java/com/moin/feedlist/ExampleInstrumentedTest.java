package com.moin.feedlist;

import android.content.Context;

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.platform.app.InstrumentationRegistry;

import com.android.volley.VolleyError;
import com.moin.feedlist.utils.ApiManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4ClassRunner.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        assertEquals("com.moin.feedlist", appContext.getPackageName());
    }

    /**
     * Test Feed API. Make sure it returns JsonArray.
     */
    @Test
    public void testFeedApi() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        //put count down latch to wait for async response to be completed
        final CountDownLatch latch = new CountDownLatch(1);
        ApiManager.getFeedApi(appContext, new ApiManager.JsonArrayCallback() {
            @Override
            public void onSuccess(JSONArray jsonArray) {
                latch.countDown();
            }
            @Override
            public void onError(VolleyError volleyError) {
                fail(volleyError.getMessage());
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test Profile API. Fetch profile for ID 2 and make sure it matches first_name.
     */
    @Test
    public void testProfileApi() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        //put count down latch to wait for async response to be completed
        final CountDownLatch latch = new CountDownLatch(1);
        ApiManager.getProfileApi(appContext, 2, new ApiManager.JsonObjCallback() {
            @Override
            public void onSuccess(JSONObject jsonObj) {
                String firstName = jsonObj.optString("first_name", "");
                assertEquals("Bill",firstName);
                latch.countDown();
            }

            @Override
            public void onError(VolleyError volleyError) {
                fail(volleyError.getMessage());
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test Task API. Fetch details for task 4 and make sure it matches poster_id to verify it.
     */
    @Test
    public void testTaskApi() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        //put count down latch to wait for async response to be completed
        final CountDownLatch latch = new CountDownLatch(1);
        ApiManager.getTaskApi(appContext, 4, new ApiManager.JsonObjCallback() {
            @Override
            public void onSuccess(JSONObject jsonObj) {
                int posterId = jsonObj.optInt("poster_id", 0);
                assertEquals(2, posterId);
                latch.countDown();
            }

            @Override
            public void onError(VolleyError volleyError) {
                fail(volleyError.getMessage());
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
