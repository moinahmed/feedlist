# Airtasker Code Test

Coding task is to build a feed viewer for Airtasker.

Feed view should end up looking like below image.

![Image](screenshots/sampleresult.png)

**This is data source for the feed:**

1. https://stage.airtasker.com/android-code-test/feed.json

You can find the rest of the data required from these end-points:

2. https://stage.airtasker.com/android-code-test/task/{taskId}.json
3. https://stage.airtasker.com/android-code-test/profile/{profileId}.json

---

## Steps to run the project.

1. Install Android Studio, at-least V3.4.
2. Install Android SDK V28.
3. Make sure buildToolsVersion "29.0.0" is intalled.
4. Clone this project.
5. Open project in Android Studio.
6. Build and run the project. (make sure gradle sync is completed)

---

## Libraries used:

Gradle will automatically sync and install them.

1. **Androidx** - for backward compatibility and support previous android versions.
2. **Volley** - to make network requests.
3. **Gson** - to parse json response from API calls.
4. **KProgressHud** - to show progress indicator.
5. **Realm** - Flat DB for storing models.

---

## Functionalities implemented

1. Optimised API request calls.
2. Supports API request retries if unable to get result from API.
3. Image caching for faster loading and to avoid extra network call.
4. Uses Recyclerview for faster tile rendering for Feed List.
5. Swipe down/ Pull down gesture to refersh Feed List.
6. Uses flat DB structure to store models.
7. Parallel asynchronous network requests.
8. Render data from DB if exists to avoid network call.
9. Basic instrumentation test cases to verify api calls and format.
10. Basic unit test case to check date-time conversion.

**Actual Output**

Screen shots are attached in **screenshots** folder. 

Check **output.png**

![Image](screenshots/output.png)

---

## Future - TODO
1. Infinite scrolling for Feed List.
2. More instrumentation and unit test cases to cover wide range of use-cases.
3. Better use of FlatDB to store models in memory + storage.

---

## Author

* Moin Ahmed - moinahmed17@gmail.com